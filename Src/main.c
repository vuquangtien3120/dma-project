/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.c
  * @brief          : Main program body
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2021 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under BSD 3-Clause license,
  * the "License"; You may not use this file except in compliance with the
  * License. You may obtain a copy of the License at:
  *                        opensource.org/licenses/BSD-3-Clause
  *
  ******************************************************************************
  */
/* USER CODE END Header */
/* Includes ------------------------------------------------------------------*/
#include "main.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#include "string.h"
#include "stdio.h"
#include "math.h"
/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */

/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */
/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/
UART_HandleTypeDef huart2;
DMA_HandleTypeDef hdma_usart2_rx;
DMA_HandleTypeDef hdma_usart2_tx;

/* USER CODE BEGIN PV */
/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
static void MX_GPIO_Init(void);
static void MX_DMA_Init(void);
static void MX_USART2_UART_Init(void);
/* USER CODE BEGIN PFP */

/* USER CODE END PFP */

/* Private user code ---------------------------------------------------------*/
/* USER CODE BEGIN 0 */

// UART variables
#define Rx_buf_size 64
#define main_buf_size 128
uint8_t Rx_buf[Rx_buf_size];
uint8_t main_buf[main_buf_size];
uint8_t Tx_buf[main_buf_size];
uint16_t oldPos = 0;
uint16_t newPos = 0;
uint16_t TxPos = 0;
int flag_send = 0;

// Modbus variables
uint8_t addr[100]; 
uint8_t CS[100];
uint16_t IN_S[100];
uint16_t DB_R[100];
int cnt,n,bufflen,startaddr,MBstat;


void HAL_UARTEx_RxEventCallback(UART_HandleTypeDef *huart, uint16_t Size)
	{
		if (huart->Instance == USART2)
	{
		oldPos = newPos;  // Update the last position before copying new data

		/* If the data in large and it is about to exceed the buffer size, we have to route it to the start of the buffer
		 * This is to maintain the circular buffer
		 * The old data in the main buffer will be overlapped
		 */
		if (oldPos + Size > main_buf_size)  // If the current position + new data size is greater than the main buffer
		{
			uint16_t datatocopy = main_buf_size - oldPos;  // find out how much space is left in the main buffer
			memcpy ((uint8_t *)main_buf + oldPos, Rx_buf, datatocopy);  // copy data in that remaining space

			oldPos = 0;  // point to the start of the buffer
			memcpy ((uint8_t *)main_buf, (uint8_t *)Rx_buf + datatocopy, (Size - datatocopy));  // copy the remaining data
			newPos = (Size - datatocopy);  // update the position
		}

		/* if the current position + new data size is less than the main buffer
		 * we will simply copy the data into the buffer and update the position
		 */
		else
		{
			memcpy ((uint8_t *)main_buf + oldPos, Rx_buf, Size);
			newPos = Size + oldPos;
		}
//		memcpy((uint8_t *)Tx_buf + TxPos,Rx_buf, Size); // copy data to the Rx buffer
//	  TxPos += Size; // Increase the Position to write next data
//		for (int i = TxPos; i < (main_buf_size + 1); i++)
//				{
//					Tx_buf[i] = '\0';
//				}
	  HAL_UARTEx_ReceiveToIdle_DMA(&huart2, (uint8_t *) Rx_buf, Rx_buf_size);
		__HAL_DMA_DISABLE_IT(&hdma_usart2_rx, DMA_IT_HT);
		flag_send = 1;
	}
	}
	void ReadFunctionCode(uint8_t buffIN[100],uint8_t buffOUT[100]) 
{
	//This is the status function, which is to obtain the status specific function of function code. 
	//Each status bit in the following function can be adjusted to function code.
	//And the corresponding functions can be added to achieve the desired operation.
	int i,j;
	MBstat = 0;
	for (i = 0; i < 20; i++)
	{
		modbus_onebit(buffIN[i]);
		
		if (MBstat == 100)
			{
			}
		else if (MBstat == 0)
			{

			}
		else if (MBstat == 1)
			{
				buffOUT[i]= buffIN[i];
			}

			//Code for function code 0x03
		else if (MBstat == 2)
			{
				buffOUT[i] = buffIN[i];
			}
		else if (MBstat == 3)
			{			
			}
		else if (MBstat == 4)
			{
				buffOUT[2] = bufflen*2;
			}
		else if (MBstat == 5)
			{
				for (j = 0,n=0; j < bufflen * 2; j= j + 2,n++)
				{
					buffOUT[3 + j] = (DB_R[n+startaddr] >> 8) & 0x00FF;
					buffOUT[3 + j+1] = DB_R[n+startaddr]& 0x00FF;	
				}
				TxPos = 3 + 2*bufflen;
				MBstat = 100;
			}
		
		//Code for function code 0x10
		else if (MBstat == 12)
			{
				buffOUT[i] = buffIN[i];
			}
		else if (MBstat == 13)
		{
			for (j = 0,n=0; j < bufflen; j= j + 2,n++)
			{		
				DB_R[n+startaddr]=(buffIN[7 + j] << 8)+buffIN[7 + j + 1];
			}
			TxPos = 6;
			MBstat = 100;
		}
		
		// Code for function code 0x01
		else if (MBstat == 22)
			{
				buffOUT[i] = buffIN[i];
			}
		else if (MBstat == 23)
			{
				if ((bufflen%8) != 0)
						buffOUT[2] = bufflen/8 + 1;
				else buffOUT[2] = bufflen/8;
			}
		else if (MBstat == 24)
			{
				for (j = 0; j < buffOUT[2]; j++)
				{
					buffOUT[3 + j] = CS[8*j+startaddr]|(CS[8*j+startaddr+1]<<1)|(CS[8*j+startaddr+2]<<2)|(CS[8*j+startaddr+3]<<3)
				 |(CS[8*j+startaddr+4]<<4)|(CS[8*j+startaddr+5]<<5)|(CS[8*j+startaddr+6]<<6)|(CS[8*j+startaddr+7]<<7);
				}
				TxPos = 3 + buffOUT[2];
				MBstat = 100;
			}
		
		// Code for function code 0x02			
		else if (MBstat == 32)
			{
			buffOUT[i] = buffIN[i];	
			}
		else if (MBstat == 33)
			{
				if ((bufflen%8) != 0)
					buffOUT[2] = bufflen/8 + 1;
				else buffOUT[2] = bufflen/8;
			}
		else if (MBstat == 34)
			{
				for (j = 0; j < buffOUT[2]; j++)
				{
					buffOUT[3 + j] = IN_S[8*j+startaddr]|(IN_S[8*j+startaddr+1]<<1)|(IN_S[8*j+startaddr+2]<<2)|(IN_S[8*j+startaddr+3]<<3)
				 |(IN_S[8*j+startaddr+4]<<4)|(IN_S[8*j+startaddr+5]<<5)|(IN_S[8*j+startaddr+6]<<6)|(IN_S[8*j+startaddr+7]<<7);
				}
				TxPos = 3 + buffOUT[2];
				MBstat = 100;
			}		
	}
	CrcCheck(buffOUT,TxPos);
	TxPos = TxPos + 2;
}

void  modbus_onebit(char a)
{
	switch (MBstat)
	{
	case 0:
		if (a == 0x01)
			MBstat = 1;
		break;
	case 1:
		if (a == 0x03)
			MBstat	= 2;
		if (a == 0x10)
			MBstat = 12;
		if (a == 0x01)
			MBstat = 22;
		if (a == 0x02)
			MBstat = 32;
		break;
	case 2:
		addr[cnt] = a;
		cnt++;
		MBstat = 3;	
		break;
	case 3:
		addr[cnt] = a;
		cnt++;
		if (cnt > 3)
		{
			cnt = 0;
			startaddr= addr[1]; //Adress which is started to write data
			bufflen= addr[3]; //Data length
			MBstat = 4;
		}
		break;
	case 4:
		MBstat = 5;
		break;

	case 6:
		MBstat = 6;
		break;
	case 7:
		MBstat = -1;
		break;
	case 8:
		MBstat = 100;
		break;
	case 100:
		break;

	case 12:
		addr[cnt]=a;
		cnt++;
		if(cnt>4)
		{
		cnt=0;
		startaddr= addr[1];
		bufflen= addr[4];
		MBstat = 13;	
		}	
		break;

		
	case 22:
		addr[cnt]=a;
		cnt++;
			if(cnt>3)
			{
			cnt=0;
			startaddr= addr[1]; // Address of first coil to be read
			bufflen= addr[3]; // Amount of coils to be read
			MBstat = 23;	
			}
		break;
	case 23:
		MBstat = 24;
		break;
	
	case 32:
		addr[cnt]=a;
		cnt++;
			if(cnt>3)
			{
			cnt=0;
			startaddr= addr[1]; // Address of first input to be read
			bufflen= addr[3]; // Amount of input to be read
			MBstat = 33;
			}
		break;
	case 33:
		MBstat = 34;
	break;


	}
}


void CrcCheck(unsigned char *buf,int len)
{
	unsigned short crc = 0xFFFF;
	unsigned char i,j=0;
	while(j < len)
	{
		crc ^= buf[j];
		for(i = 0; i < 8; i++)
		{
			if(crc & 0x01)
				{
				crc >>= 1;
				crc ^= 0xA001;
				}
			else
			crc >>= 1;
		}
		j++;
	}
	buf[j] = crc % 0x100;
	buf[j+1]=crc / 0x100;
}
		
/* USER CODE END 0 */

/**
  * @brief  The application entry point.
  * @retval int
  */
int main(void)
{
  /* USER CODE BEGIN 1 */
	DB_R[0] = 0x0102; //Initial value for DB_R
	DB_R[1] = 0x0204;
	DB_R[2] = 0x0306;
	DB_R[3] = 0x0408;

  /* USER CODE END 1 */

  /* MCU Configuration--------------------------------------------------------*/

  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
  HAL_Init();

  /* USER CODE BEGIN Init */

  /* USER CODE END Init */

  /* Configure the system clock */
  SystemClock_Config();

  /* USER CODE BEGIN SysInit */

  /* USER CODE END SysInit */

  /* Initialize all configured peripherals */
  MX_GPIO_Init();
  MX_DMA_Init();
  MX_USART2_UART_Init();
  /* USER CODE BEGIN 2 */
  HAL_UARTEx_ReceiveToIdle_DMA(&huart2, (uint8_t *) Rx_buf, Rx_buf_size);
  __HAL_DMA_DISABLE_IT(&hdma_usart2_rx, DMA_IT_HT);
  /* USER CODE END 2 */

  /* Infinite loop */
  /* USER CODE BEGIN WHILE */
  while (1)
  {
		HAL_Delay(100);
		if(flag_send)
			{
				ReadFunctionCode(Rx_buf,Tx_buf);
				HAL_UART_Transmit_DMA(&huart2, (uint8_t *)Tx_buf,TxPos);
				TxPos = 0;
				flag_send = 0;
				__HAL_UART_ENABLE_IT(&huart2, UART_IT_TC);
			}
    /* USER CODE END WHILE */

    /* USER CODE BEGIN 3 */
  }
  /* USER CODE END 3 */
}

/**
  * @brief System Clock Configuration
  * @retval None
  */
void SystemClock_Config(void)
{
  RCC_OscInitTypeDef RCC_OscInitStruct = {0};
  RCC_ClkInitTypeDef RCC_ClkInitStruct = {0};

  /** Configure the main internal regulator output voltage
  */
  __HAL_RCC_PWR_CLK_ENABLE();
  __HAL_PWR_VOLTAGESCALING_CONFIG(PWR_REGULATOR_VOLTAGE_SCALE1);
  /** Initializes the RCC Oscillators according to the specified parameters
  * in the RCC_OscInitTypeDef structure.
  */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSE;
  RCC_OscInitStruct.HSEState = RCC_HSE_ON;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSE;
  RCC_OscInitStruct.PLL.PLLM = 8;
  RCC_OscInitStruct.PLL.PLLN = 336;
  RCC_OscInitStruct.PLL.PLLP = RCC_PLLP_DIV2;
  RCC_OscInitStruct.PLL.PLLQ = 4;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    Error_Handler();
  }
  /** Initializes the CPU, AHB and APB buses clocks
  */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV4;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV2;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_5) != HAL_OK)
  {
    Error_Handler();
  }
}

/**
  * @brief USART2 Initialization Function
  * @param None
  * @retval None
  */
static void MX_USART2_UART_Init(void)
{

  /* USER CODE BEGIN USART2_Init 0 */

  /* USER CODE END USART2_Init 0 */

  /* USER CODE BEGIN USART2_Init 1 */

  /* USER CODE END USART2_Init 1 */
  huart2.Instance = USART2;
  huart2.Init.BaudRate = 115200;
  huart2.Init.WordLength = UART_WORDLENGTH_8B;
  huart2.Init.StopBits = UART_STOPBITS_1;
  huart2.Init.Parity = UART_PARITY_NONE;
  huart2.Init.Mode = UART_MODE_TX_RX;
  huart2.Init.HwFlowCtl = UART_HWCONTROL_NONE;
  huart2.Init.OverSampling = UART_OVERSAMPLING_16;
  if (HAL_UART_Init(&huart2) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN USART2_Init 2 */

  /* USER CODE END USART2_Init 2 */

}

/**
  * Enable DMA controller clock
  */
static void MX_DMA_Init(void)
{

  /* DMA controller clock enable */
  __HAL_RCC_DMA1_CLK_ENABLE();

  /* DMA interrupt init */
  /* DMA1_Stream5_IRQn interrupt configuration */
  HAL_NVIC_SetPriority(DMA1_Stream5_IRQn, 0, 0);
  HAL_NVIC_EnableIRQ(DMA1_Stream5_IRQn);
  /* DMA1_Stream6_IRQn interrupt configuration */
  HAL_NVIC_SetPriority(DMA1_Stream6_IRQn, 0, 0);
  HAL_NVIC_EnableIRQ(DMA1_Stream6_IRQn);

}

/**
  * @brief GPIO Initialization Function
  * @param None
  * @retval None
  */
static void MX_GPIO_Init(void)
{

  /* GPIO Ports Clock Enable */
  __HAL_RCC_GPIOH_CLK_ENABLE();
  __HAL_RCC_GPIOA_CLK_ENABLE();

}

/* USER CODE BEGIN 4 */

/* USER CODE END 4 */

/**
  * @brief  This function is executed in case of error occurrence.
  * @retval None
  */
void Error_Handler(void)
{
  /* USER CODE BEGIN Error_Handler_Debug */
  /* User can add his own implementation to report the HAL error return state */
  __disable_irq();
  while (1)
  {
  }
  /* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t *file, uint32_t line)
{
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
     ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
